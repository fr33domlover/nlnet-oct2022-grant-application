# Potential areas of work

1. Develop a test program, that launches a set of instances, submits
   ActivityPub activities to get them to communicate, and watches the results
   (essentially a client program, but automated like a bot, rather than having
   a full UI for humans)

Candidates: (none)

2. **Progress with the ForgeFed spec**

Candidates: **fr33domlover**

3. Design and develop a GUI client app for Vervis, and that could also be used
   with other forges

Candidates: (none)

4. **Continue implementing ForgeFed in Vervis**

Candidates: **fr33domlover**

5. Implement federation in GitLab, even if as a fork / in heptapod

Candidates: (none)

6. Implement federation in another forge, such as Phorge

Candidates: (none)

7. Define and implement a distributed semantic project search system (e.g. DHT
   based / distributed graph DB based), allowing to search, sort and filter
   projects by name, by programming languages, by activity, by VCS type, by
   author, by license, etc. etc.

Candidates: (none)

8. Define and implement a data replication mechanism for forges, possibly
   adding "replication storage provider" as an actor type in ForgeFed

Candidates: (none)

9. Define and implement a demo of fully-P2P federation using a distributed
   actor model capability system such as Spritely, including p2p human trust
   mechanism such as a petname system (like in Scuttlebutt and in Spritely)

Candidates: (none)

10. Design and develop a Nextcloud integration plugin, e.g. using ForgeFed for
    the task/desk apps; displaying repos in the Files app; hosting software
    project releases and other artifacts

Candidates: (none)

11. Decentralized integrated project funding system

Candidates: (none)

# Area 2: ForgeFed spec

## Tasks

1. **C2S**

Status: TODO

2. **Integrated funding info and funding system**

Status: TODO

3. **Following, addressing, verified inbox forwarding**: defining the process
   of following forge-related objects and actors; the required or recommended
   recipients for the various activities; a mechanism for verified ActivityPub
   inbox forwarding

Status: WIP by fr33domlover

4. **Ticket dependencies**: defining the process and federation of creating
   ticket dependencies, hosting, and deleting them.

Status: WIP by fr33domlover

5. **Ticket updates and flow**: defining how modifications of tickets are made,
   communicated and federated.

Status: WIP by fr33domlover

6. **MRs and patche updates/flow & code review**: defining how modifications of
   patches and Merge Requests are made, communicated and federated, including
   new versions and including code review.

Status: Initial WIP by fr33domlover

7. **UI translation**: defining how software user interface localization
   management services (such as Weblate) would be represented and interacted
   with in a federated manner, and how they would interact with other forge
   objects, such as repositories and tickets.

Status: TODO

8. **VCS remote push**: defining how users’ SSH keys would be uploaded,
   represented and published, and the process of announcing the pushing of
   commits to remotely-hosted repositories (i.e. ones hosted on different
   servers than the user’s home instance) in a verifiable way.

Status: TODO

9. **Wiki**: defining how wikis and pages would be created, modified and
   deleted, and how wikis are integrated with other project tools (eg.
   repository or tickets). This feature should integrate XWiki if possible,
   since they are working on wiki federation.

Status: TODO

10. **Search, discovery, WebFinger, instance exploration**: defining how search
    and discovery of projects across the federated network would work,
    whether/how the different types of actors would be represented in
    WebFinger, and a representation of the public projects and content that a
    server hosts.

Status: TODO

11. **Teams, groups, roles, access control**: defining how federated teams and
    projects would be created and modified, how roles would be assigned to
    members, and how access controls would be defined, enforced and announced.

Status: Advanced WIP by fr33domlover

12. **CI/CD**: defining how Continuous Integration and Deployment services
    would communicate and interact with repositories.

Status: TODO

13. **Releases, packaging, package repos**: defining how packaged releases of
    repository content or content derived from it would be created, published
    and manipulated, and linked with automatic CI builds.

Status: TODO

14. **Kanban**: defining how kanbans and their contents would be created,
    managed and federated.

Status: TODO

15. **Forks, stars, software specific vocabulary for repos**: defining how
    repositories could provide properties such as license, programming
    language, dependencies, etc., and how forking and starring of repositories
    is done, represented and communicated.

Status: TODO

16. **Verifiable builds**: defining how software builds would provide build
    recipes and hashes of the results to allow for verifiable reproducible
    builds.

Status: TODO

17. **GPG key publishing and commit/tag signature verification**: defining how
    a user’s personal GPG keys would be uploaded, published, used for verifying
    cryptographic signatures on commits and tags within repositories, and used
    for signing other kinds of objects such as tickets.

Status: TODO

18. **VCS specifics**: defining the aspects of ForgeFed specific or unique to
    each version control system, in particular Git and Darcs, and how to use
    ForgeFed with these version control systems. If there’s interest and
    feedback from the community, add Mercurial and/or SVN as well.

Status: TODO

19. **Migration/Import**: defining how to migrate (or “import”) a project from
    one instance to another including tickets, patches, wikis, and every other
    objects pertaining to the project. Migrations will allow complete “forks”
    of projects, and eliminate vendor lock-in.

Status: TODO

20. **Pages**

Status: TODO

21. **Federated forum and blog for software projects**

Status: TODO

22. **Container image registries**

Status: TODO

23. **Project information** vocabulary e.g. DOAP based; specify a project's
    website, forum, blog, irc/matrix rooms, fediverse accounts, mailing list,
    newsletter, documentation... etc. etc.

Status: TODO

24. **Snippets, paste, real-time collaborative editing integration**

Status: TODO

25. Integrated federated project funding system

Status: TODO

## Drafts

This is a very rough early plan from 2019, provided for reference.

Draft 1:

- Representation of tickets and dependencies (DONE)
- Representation of MRs and patches (WIP)
- Discussion, comments, replies (DONE)
- Opening a ticket (DONE)
- **[S1]** Opening a MR/patch (WIP)
- VCS local push (WIP, ALMOST DONE)

Draft 2:

- **[S2]** C2S for the features from Draft 1
- **[S3]** Following, addressing, inbox forwarding (WIP)
- **[S4]** Ticket dependencies: creation, deletion, hosting
- **[S5]** Ticket updates/flow
- **[S6]** MR/patch updates/flow
- **[S7]** SSH key publishing (WIP)
- **[S8]** VCS remote push (WIP)
- **[S23]** Forks and stars

Draft 3:

- Ticket tracking remaining features
- Patches, MRs & code review remaining features
- Automatic patch listing a-la DarcsHub
- **[S9]** Wiki
- **[S10]** Search, discovery, WebFinger, instance exploration
- **[S11]** Teams, groups, roles, access control

Draft 4:

- **[S12]** CI, CD
- **[S13]** Releases, packaging, package repos
- **[S14]** Kanban
- **[S15]** Agile dev sprints etc.
- **[S16]** Software specific vocab, e.g. specifying the programming language, license, etc. of repos
- Reuse and integration of existing vocabs such as DOAP
- **[S17]** Verifiable builds

Extra:

- **[S18]** OCAPs
- **[S19]** GPG key publishing and commit/tag signature verification
- **[S20]** Reusable vocabulary and protocol components
- **[S21]** Git specifics
- **[S22]** Darcs specifics
- Mercurial specifics
- Fossil specifics? Would Fossil benefit from ForgeFed?
- Email integration

# Area 3: UI for Vervis

List of pages/features to do UI for, from 2019:

* Main page
* Ticket
* MRs and patches
* Discussion
* User profile and settings
* Repo files
* Repo history
* Repo commit
* User activity
* Repo activity
* Group
* Project
* Following
* SSH and GPG keys
* Wiki view
* Wiki edit
* Public page, firehose, discovery
* Search
* Collaborators, roles, access controls
* Releases and history
* Notifications
* Assigned tickets, claim requests
* Workflow
* Ticket tree

# Area 4: Vervis

Rough early plan from 2019 provided for reference:

Draft 1:

- **[IV1]** Representation of tickets and dependencies
- **[IV2]** Representation of MRs and patches
- Discussion, making a comment
- **[IV3]** Opening a ticket
- **[IV4]** Opening a MR/patch
- VCS local push

Draft 2:

- C2S for the features from Draft 1
- Following, addressing, inbox forwarding
- **[IV5]** Ticket dependencies: creation, deletion, hosting
- **[IV6]** Ticket updates/flow
- **[IV7]** MR/patch updates/flow
- **[IV8]** SSH key publishing
- **[IV9]** VCS remote push
- **[IV10]** Forks and stars

Draft 3:

- Ticket tracking remaining features
- Patches, MRs & code review remaining features
- Automatic patch listing a-la DarcsHub
- **[IV11]** Wiki
- **[IV12]** Search, discovery, WebFinger, instance exploration
- **[IV13]** Teams, groups, roles, access control

Draft 4:

- CI, CD
- **[IV14]** Releases, packaging, package repos
- Kanban
- Agile dev sprints etc.
- **[IV15]** Software specific vocab, e.g. specifying the programming language, license, etc. of repos
- Reuse and integration of existing vocabs such as DOAP
- Verifiable builds

Extra:

- **[IV16]** OCAPs
- **[IV17]** GPG key publishing and commit/tag signature verification
- Reusable vocabulary and protocol components
- Git specifics
- Darcs specifics
- Mercurial specifics
- Fossil specifics? Would Fossil benefit from ForgeFed?
- Email integration
- **[CV1]** Discoverability of instances via NodeInfo/ServiceInfo and the fediverse server discovery websites

# Areas 5 and 6: Implementing in another forge

Here's a draft from 2019, originally for Gitea:

Draft 1:

- **[IG1]** Representation of tickets and dependencies
- **[IG2]** Representation of MRs and patches
- **[IG3]** Discussion, making a comment
- **[IG4]** Opening a ticket
- **[IG5]** Opening a MR/patch
- **[IG6]** VCS local push

Draft 2:

- C2S for the features from Draft 1
- **[IG7]** Following, addressing, inbox forwarding
- **[IG8]** Ticket dependencies: creation, deletion, hosting
- **[IG9]** Ticket updates/flow
- **[IG10]** MR/patch updates/flow
- **[IG11]** SSH key publishing
- **[IG12]** VCS remote push
- **[IG13]** Forks and stars

Draft 3:

- Ticket tracking remaining features
- Patches, MRs & code review remaining features
- Automatic patch listing a-la DarcsHub
- **[IG14]** Wiki
- **[IG15]** Search, discovery, WebFinger, instance exploration
- **[IG16]** Teams, groups, roles, access control

Draft 4:

- CI, CD
- **[IG17]** Releases, packaging, package repos
- Kanban
- Agile dev sprints etc.
- **[IG18]** Software specific vocab, e.g. specifying the programming language, license, etc. of repos
- Reuse and integration of existing vocabs such as DOAP
- Verifiable builds

Extra:

- **[IG19]** OCAPs
- **[IG20]** GPG key publishing and commit/tag signature verification
- Reusable vocabulary and protocol components
- Git specifics
- Darcs specifics
- Mercurial specifics
- Fossil specifics? Would Fossil benefit from ForgeFed?
- Email integration
