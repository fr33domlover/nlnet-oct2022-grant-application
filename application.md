# (1) Abstract

ForgeFed is a vocabulary and protocol for decentralized communication and
federation of websites used for hosting and collaboration on version control
repositories, issue tracking and project management.

These websites and services include:

- Software collaboration forges such as Gitea, GitLab, github
- Project and task tracking services such as OpenProject, Redmine, Taiga; even
  government services that allow citizens to collaborate on decisions and
  plans, such as in civic planning
- Code review services like Gerrit
- Software development services like CI/CD (e.g. Drone CI, Woodpecker CI,
  travis), UI translation services (e.g. Weblate, poeditor), static website
  hosting (e.g. GitLab Pages, github pages)

The ForgeFed protocol specification is already in progress. We expect to
publish a first official draft version for the wide FLOSS community to
implement; To continue to implement every addition to the specification in
Vervis, the reference implementation forge, and in other software components
we'll develop as needed; to continue working together with the teams
implementing federation in other forges (Gitea, etc.), adapting the
specification to their input and guiding them whenever necessary; to continue
evolving the specification with the wider FLOSS community.

ForgeFed is an extension of ActivityPub, and web apps implementing it would be
joining the Fediverse. The world of repo and project hosting would switch from
the centralized model of githu8 (and the lonely disconnected websites running
GitLab or Gitea etc.) into a network of federating websites, creating a global
decentralized community.

# (2) Involvement

fr33domlover:

I've been involved in *this* project, for a few years now. In 2016 I started
developing Vervis, a forge with federation built into its core. In 2018, I
joined the ForgeFed team from the very beginning, and started implementing
ActivityPub based federation into Vervis. In 2019, me and another ForgeFed team
member applied for NLNet funding for ForgeFed, and made progress with the
specification. After a hiatus, in 2022 I resumed my work on ForgeFed and Vervis,
making considerable progress in both the specification and the Vervis
implementation.

While I'm not directly involved in the Gitea project, I'm working closely with
the people implementing federation in Gitea, and some of them are also members
of the ForgeFed project.

I've been a free software and free internet advocate for many years now. In the
Peers community (https://peers.community), we laucnhed the NotABug.org forge
website some years ago, intending to provide an alternative to centralized
services like github.

# (3) Costs

The project had funding from NLnet via the NGI0 program (Project number
2019-10-051), which is now ending in October 2022. And it is why we are asking
for more funding now. ForgeFed doesn't have other funding sources.

We intend to use the whole 50k EUR for human labor, i.e. our time spent working
on the project.

# (4) Compare

Related projects are:

- The Fediverse: Network of servers federated using ActivityPub. Existing
  federated web apps provide social networking, blogging, microblogging, image
  sharing, but nothing related to project mamagement or repository hosting.
  ForgeFed is an ActivityPub extension, bringing federation to the domain of
  software project hosting.

- Forges like Gitea and Gitlab CE are free software, and can be self-hosted,
  but they create separate islands that aren't interconnected. It's not easy to
  interact and collaborate across forge websites, and there's no global index
  or search engine for browsing all the free software projects out there, and
  no tools for creating a self-organizing global FLOSS community. ForgeFed
  allows the forge websites to federate and form a single community powered
  network.

- Forge websites like Codeberg.org and Framagit.org: These websites offer
  Gitea/GitLab as a free service to the free software community, but as these
  websites grow more and more, they don't have tools to prevent centralization
  and created a decentralized network. ForgeFed will allow them to federate on
  the ActivityPub Fediverse network.

- Radicle.xyz: An attempt to create a peer-to-peer software project
  collaboration model, that doesn't need forge websites at all. This is a
  different approach to solving the problems ForgeFed is trying to solve, which
  has different strengths and weaknesses. Radicle is specific to the Git
  version control system, requires extra network and disk space for replication
  of other people's projects, and (to our knowledge) currently has a fixed data
  model that doesn't allow extension via arbitrary Linked Data. A peer-to-peer
  network is creating a whole new system separate from the regular HTTP-based
  forge servers, and also doesn't provide services like Continuous Integration,
  which requires intensive computation, and storage of large objects such as
  Docker container images. ForgeFed is more traditionally server-based, and can
  be implemented in popular existing forges like Gitea and GitLab, and can
  support any VCS, not limited to Git. ForgeFed is primarily a communication
  protocol, and allows freedom to choose any suitable storage mechanism, while
  Radicle is both a protocol and a data format. ForgeFed and Radicle are
  different approaches and time will tell how they develop and if they can
  cooperate.

- git-ssb: An attempt to host Git repositories in the p2p gossip-based
  Scuttlebutt network. Similar concept to Radicle.

# (5) Challenges

- Define a vocabulary that would cover the needs and use cases of the various
  project hosting and collaboration platforms, work with them to understand
  their needs, generalize the vocabulary to be expressive enough for everyone
  and still easy to use

- Understand the deep implications on security, authenticity and trust on a
  decentralized forge network, especially when the authenticity and integrity
  of software is involved

- While implementing ForgeFed in Vervis, handle the complexity of a
  decentralized environment and web frameworks and tools that aren't suited to
  federated web applications, develop and extend the toold needed for effective
  development for a federated forge server application

- Explore and understand the needs and complexities of a User Interface of a
  federated web app, specifically a federated forge, how to authenticate data
  coming from remote untrusted servers and display it in a secure way

- Create great documentation to accompany the specification; good
  implementation guides are something the Fediverse community is lacking,
  especially because it's a new technology and still developing and maturing

- Achieve cooperation and interoperability between the different
  implementations that use different programming languages, different
  libraries, different internal data models

- A software project is a strongly consistent dataset (for instance a
  pull request is composed of commits, reviews, comments, reactions
  etc.). ActivityPub is not designed to provide any guarantee
  regarding consistency: it needs to rely on other protocols and data
  formats for that purpose.

# (6) Ecosystem

- Federated web application developer community and W3C social web community
  group: We talk with them, primarily via the SocialHub forum (at
  https://socialhub.activitypub.rocks) and seek their advice and opinion on
  technical decisions and aspects, and we'll continue working with them and
  consulting with them

- Gitea developers: We cooperate with them to help with implementing the
  ForgeFed protocol in their software

- GitLab developers: So far there's no visible progress with federation in
  GitLab, but we'll initiate discussion with them and try to gather the
  community to ask and seek their cooperation, advocating for forge federation
  and the importance of it, and communicate about the possibility of working
  together with them to implement ForgeFed in GitLab CE

- Forge users, FOSS community: We've been publishing updates about our work on
  the fediverse and talking about it on IRC, getting feedback, understanding
  what people need the most. A blog will launch soon too. We'll be looking for
  motivated people to participate in the implementation of ForgeFed in the
  variety of existing relevant software projects. We'll encourage forge
  developers to accept such contributions in the FOSS community.

- The Friendly Forge Format (F3) is an emerging file format to
  represent software projects at rest and we'll work with them to
  ensure it plays well with ForgeFed. F3 provides strong consistency
  that ForgeFed can reference and allows a software forge to obtain
  additional data about a software project using an alternative
  protocol such as git.

- Software Freedom Conservancy and the GiveUpGithub.org campaign: It if becomes
  relevant, we'll try to collaborate with them on helping the free software
  community move from github to free and federated forges

- Codeberg.org and other community forge websites: We'll collaborate with them
  as necessary, to bring federation features to their platform
